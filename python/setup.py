""" setup.py for Felsius"""
from distutils.core import setup

setup(
    name='felsius',
    description='Convert to the temperature scale of tomorrow!',
    version='1.0.1',
    license='MIT',
    long_description='Convenience library to convert to (and from) the '
                     'temperature unit of the future, today!',
    author='Torro',
    author_email='Torro@tuta.io',
    url='https://github.com/Torro/felsius/tree/master/python',
    packages=['.'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Education',
        'Topic :: Software Development :: Internationalization',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities'
    ]
)
