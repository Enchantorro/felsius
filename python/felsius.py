"""Convert to/from Felsius"""


def from_celsius(degrees_celsius):
    """Convert from Celsius to Felsius."""
    return (degrees_celsius * 7 / 5) + 16


def to_celsius(degrees_felsius):
    """Revert Felsius to Celsius."""
    return (degrees_felsius - 16) * 5 / 7


def from_fahrenheit(degrees_fahrenheit):
    """Convert from Fahrenheit to Felsius."""
    return (7 * degrees_fahrenheit - 80) / 9


def to_fahrenheit(degrees_felsius):
    """Revert Felsius to Fahrenheit."""
    return(9 * degrees_felsius + 80) / 7
